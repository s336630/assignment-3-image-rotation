#ifndef FILE_H
#define FILE_H

#include <stdio.h>

enum file_open_status {
    OPEN_OK,
    OPEN_ERROR
};

enum file_open_status open_file_to_read(char* const filename, FILE** file);

enum file_open_status open_file_to_write(char* const filename, FILE** file);
#endif
