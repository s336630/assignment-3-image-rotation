#ifndef IMAGE_H
#define IMAGE_H
#include <inttypes.h>
#include <stdlib.h>

struct image {
    long width, height;
    struct pixel* pixels;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

#endif
