#include <bmp.h>
#include <file.h>
#include <rotating.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc < 3) {
        puts("Arguments count should be 3\n");
    } else {
        FILE* in = NULL;
        FILE* out = NULL;
        enum file_open_status open_status = open_file_to_read(argv[1], &in);
        enum file_open_status write_status = open_file_to_write(argv[2], &out);
        if (open_status != OPEN_OK) {
            puts("Failed to open file to read\n");
            return 1;
        }

        //Creating image and filling it 
        struct image img = {0};
        if (from_bmp(in, &img) != READ_OK) {
            puts("Failed to read from image\n");
            return 1;
        }

        //trying to close input file 
        if (fclose(in) != 0) {
            puts("Failed to close file to read\n");
            return 1;
        }

        //rotating
        struct image rotated_img = rotate(&img);
        free(img.pixels);           //destroying image

        if (write_status != OPEN_OK) {
            puts("Failed to open file to write\n");
            return 1;
        }

        
        if (to_bmp(out, &rotated_img) != WRITE_OK) {
            puts("Failed to write file");
            return 1;
        }

        if (fclose(out) != 0) {
            puts("Failed to close output");
            return 1;
        }

        free(rotated_img.pixels);
    }
}
