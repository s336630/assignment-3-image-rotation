#include "rotating.h"
#include <inttypes.h>
#include <stdlib.h>

struct image rotate( struct image* const source ) {
    struct image rotated = {            // creating new image
        .width = source->height,
        .height = source->width,
        .pixels = malloc(source->width*source->height*sizeof(struct pixel))
    };

    for (uint64_t y = 0; y < source->height; y++) {
        for(uint64_t x = 0; x < source->width; x++) {
            rotated.pixels[y + rotated.width * (source->width - x - 1)] = source->pixels[x + y * source->width];
        }
    }

    return rotated;
}
