#include "file.h"

enum file_open_status open_file_to_read(char* const filename, FILE** restrict file) {
	*file = fopen(filename, "rb");
	if (*file){
		return OPEN_OK;
	} else {
		return OPEN_ERROR;
	}
}

enum file_open_status open_file_to_write(char* const filename, FILE** restrict file) {
	*file = fopen(filename, "wb");
	if (*file){
		return OPEN_OK;
	} else {
		return OPEN_ERROR;
	}
}
