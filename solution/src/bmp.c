#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define SIGNATURE   19778         // "BM"
#define RESERVED    0
#define OFFSET      54
#define H_SIZE      40
#define PLANES      1
#define BIT_COUNT   24
#define COMPRESSION 0
#define PPM         1000
#define CLRS_USED   0
#define CLRS_IMPT   0

static long calculate_padding(long width) {
    long d = (width * 3) % 4;
    if (d == 0) {
        return 0;
    } else {
        return 4 - d;
    }
}

enum read_status from_bmp( FILE* in, struct image* img ) {    
    // Read file header
    struct bmp_header header = {0};
    if (fread(&header, sizeof (struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    // Verify signature in header
    if (header.bfType != SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }
    
    //Prepared image (width, height, pixels)
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->pixels = malloc(sizeof(struct pixel) * img->width * img->height);

    const long padding = calculate_padding(img->width);

    // fill in the image
    for (int y = 0; y < img->height; y++) {
        if (fread(
                &(img->pixels[(img->height - y - 1) * img->width]),      // get last row to fill
                sizeof(struct pixel),
                img->width,
                in
                ) != img->width
            ) {
            return READ_INVALID_BITS;
        }
        
        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    // Generate header
    struct bmp_header header = {
        .bfType=SIGNATURE,
        .bfileSize=sizeof(struct bmp_header) + img->width*img->height*sizeof(struct pixel),
        .bfReserved=RESERVED,
        .bOffBits=sizeof(struct bmp_header),
        .biSize=H_SIZE,
        .biWidth=img->width,
        .biHeight=img->height,
        .biPlanes=PLANES,
        .biBitCount=BIT_COUNT,
        .biCompression=COMPRESSION,
        .biSizeImage=0,
        .biXPelsPerMeter=PPM,
        .biYPelsPerMeter=PPM,
        .biClrUsed=CLRS_USED,
        .biClrImportant=CLRS_IMPT
    };

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    const long padding = calculate_padding(img->width);
    const int8_t null_bytes[] = {0, 0, 0};

    for (int y = 0; y < img->height; y++) {
        if (fwrite(
                &(img->pixels[(img->height - y - 1) * img->width]),
                sizeof(struct pixel),
                img->width,
                out
            ) != img->width
        ) {
            return WRITE_ERROR;
        } 

        if (fwrite(null_bytes, sizeof(uint8_t), padding, out) != padding) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
